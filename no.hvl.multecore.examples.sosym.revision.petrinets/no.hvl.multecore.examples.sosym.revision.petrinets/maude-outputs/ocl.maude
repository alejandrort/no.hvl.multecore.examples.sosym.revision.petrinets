sload mlm-core.maude

sload ocl-prelude.maude
mod OCL-SIGN is
	pr OCL-PRELUDE .
	pr MLM-NAME .
	pr INT .

	sorts OclExp .
	subsort OclBool OclInt OclString OclFloat < OclExp .

	op _._-> size`(`) : Name Name -> OclExp .
	op _._ : Name Name -> OclExp .

	var OclNzN : OclNzNat .

	op ocl2Maude : OclInt -> Int .
	eq ocl2Maude(0) = 0 .
	eq ocl2Maude(s 0) = 1 .
	eq ocl2Maude(s OclNzN) = s ocl2Maude(OclNzN) .
	eq ocl2Maude(-OclInt OclNzN) = - ocl2Maude(OclNzN) .
endm

mod OCL{V :: TRIV} is
	pr OCL-SIGN .
	pr MLM-CORE{V} .

	op value2ocl : V$Elt -> OclExp .

	op eval : OclExp Configuration -> Int .
	eq eval(OCLE, Conf) = ocl2Maude(evalOcl(OCLE, Conf)) .

	vars NM N N' Type PN PN' : Name .
	vars sourceName typeName matchedTypeName : String .
	vars Conf Conf' Rels Elts : Configuration .
	vars sourceLevel typeLevel matchedTypeLevel : Nat .
	vars Oid' O1 O2 : Oid .
	vars Atts Atts' Atts'' : AttributeSet .
	var OCLE : OclExp .
	var V : V$Elt .
	op evalOcl : OclExp System -> OclExp .
	op evalOcl : OclExp Configuration -> OclExp .
	eq evalOcl(OCLE, { Conf }) = evalOcl(OCLE, Conf) .
	eq evalOcl(id(sourceLevel, sourceName) . id(typeLevel, typeName) -> size(),
		< level(sourceLevel) : Model |
			rels : (
				----matched relation (here we could have another kind of arc, which still is an instance of the type given as parameter)
				< Oid' : Relation | type : id(matchedTypeLevel, matchedTypeName), source : id(sourceLevel, sourceName), Atts' >
				Rels),
			Atts'' >
		Conf')
		----If the matched type is the one we are looking for
		= if id(matchedTypeLevel, matchedTypeName) == id(typeLevel, typeName)
		---- or it is transitively we sum 1 and continue searching
		or-else *(id(matchedTypeLevel, matchedTypeName), level(matchedTypeLevel), id(typeLevel, typeName), Conf')
			then (1).OclNzNat
			else (0).OclZero
			fi
			+ evalOcl(id(sourceLevel, sourceName) . id(typeLevel, typeName) -> size(),
				< level(sourceLevel) : Model |
					rels : Rels,
					Atts'' >
					Conf') .
	eq evalOcl(NM . Type -> size(), < level(sourceLevel) : Model | rels : Rels, Atts > Conf) = 0 [owise] .

	eq evalOcl(PN . PN',
		Conf
			< level(4) : Model |
				elts : < O1 : Node | name : PN,
					attributes : < O2 : Attri | type : PN', nameOrValue : V >,
					Atts >
				Elts,
			Atts' >)
		= value2ocl(V) .
	endm

view Ocl from TRIV to OCL-SIGN is
	sort Elt to OclExp .
endv

